# Compendium of pt_BR.
msgid ""
msgstr ""
"Project-Id-Version: compendium-pt_BR\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2018-05-15 19:31-0300\n"
"PO-Revision-Date: 2018-05-15 19:52-0300\n"
"Language-Team: Koha Translation Team \n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: application/x-publican; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../source/lists.rst:4
msgid "Lists & Cart"
msgstr ""

#: ../../source/lists.rst:6
msgid ""
"Lists are a way to save a collection of content on a specific topic or for a "
"specific purpose. The Cart is a session specific storage space."
msgstr ""

#: ../../source/lists.rst:9
#, fuzzy
msgid "*Get there:* More > Lists"
msgstr "Ir para: "

#: ../../source/lists.rst:14
msgid "Lists"
msgstr "Listas"

#: ../../source/lists.rst:19
#, fuzzy
msgid "Create a List"
msgstr "Criar um conjunto"

#: ../../source/lists.rst:21
msgid ""
"A list can be created by visiting the Lists page and clicking 'New List'"
msgstr ""
"Uma lista pode ser criada visitando a página de Listas e clicando em 'Nova "
"Lista'"

#: ../../source/lists.rst:23
msgid "|image868|"
msgstr ""

#: ../../source/lists.rst:25
msgid "The new list form offers several options for creating your list:"
msgstr ""
"O formulário de nova lista oferece diversas opções para criar sua lista:"

#: ../../source/lists.rst:27
msgid "|image869|"
msgstr ""

#: ../../source/lists.rst:29
msgid "The name is what will appear on the list of Lists"
msgstr "O nome que irá aparecer na lista de Listas"

#: ../../source/lists.rst:31
msgid "You can also choose how to sort the list"
msgstr "Você pode também escolher como ordenar a lista"

#: ../../source/lists.rst:33
msgid "Next decide if your list is going to be private or public"
msgstr ""

#: ../../source/lists.rst:35
#, fuzzy
msgid ""
"A Private List is managed by you and can be seen only by you (depending on "
"your permissions settings below)"
msgstr "Uma lista privada é administrada e só pode ser vista por você"

#: ../../source/lists.rst:38
#, fuzzy
msgid ""
"A Public List can be seen by everybody, but managed only by you (depending "
"on your permissions settings below)"
msgstr ""
"A Lista Pública  pode ser vista por todos, mas apenas administrada por você."

#: ../../source/lists.rst:41
msgid ""
"Finally decide what your permissions will be on the list. You can allow or "
"disallow:"
msgstr ""

#: ../../source/lists.rst:44
#, fuzzy
msgid "anyone else to add entries"
msgstr "ninguém para adicionar entradas."

#: ../../source/lists.rst:46
#, fuzzy
msgid "anyone to remove his own contributed entries"
msgstr "alguém para remover outras entradas de contribuições."

#: ../../source/lists.rst:48
#, fuzzy
msgid "**Note**"
msgstr "Nota"

#: ../../source/lists.rst:50
msgid ""
"The owner of a list is always allowed to add entries, but needs permission "
"to remove."
msgstr ""
"O proprietário de uma lista sempre tem permissões para adicionar entradas, "
"mas precisa de permissão para removê-las."

#: ../../source/lists.rst:53
#, fuzzy
msgid "anyone to remove other contributed entries"
msgstr "alguém para remover outras entradas de contribuições."

#: ../../source/lists.rst:55
msgid "A list can also be created from the catalog search results"
msgstr ""
"Uma lista também pode ser criada a partir dos resultados de busca no catálogo"

#: ../../source/lists.rst:57
msgid "|image870|"
msgstr ""

#: ../../source/lists.rst:59
msgid "Check the box to the left of the titles you want to add to the new list"
msgstr ""
"Clique na caixa de seleção no lado esquerdo do título que você deseja "
"adicionar a nova lista"

#: ../../source/lists.rst:62
#, fuzzy
msgid "Choose [New List] from the 'Add to:' pull down menu"
msgstr "Selecione [Nova Lista] no menu 'Adicionar para': exibir o menu "

#: ../../source/lists.rst:64
msgid "|image871|"
msgstr ""

#: ../../source/lists.rst:66
#, fuzzy
msgid "Name the list and choose what type of list this is"
msgstr "Nomeie a lista e escolha que tipo de lista é "

#: ../../source/lists.rst:68
msgid "A Private List is managed by you and can be seen only by you"
msgstr "Uma lista privada é administrada e só pode ser vista por você"

#: ../../source/lists.rst:70
msgid "A Public List can be seen by everybody, but managed only by you"
msgstr ""
"A Lista Pública  pode ser vista por todos, mas apenas administrada por você."

#: ../../source/lists.rst:72
msgid ""
"Once the list is saved it will accessible from the Lists page and from the "
"'Add to' menu at the top of the search results."
msgstr ""
"Uma vez que a lista foi salva ela estará acessível na página Listas e no "
"menu 'Adicionar para' no topo dos resultados de busca."

#: ../../source/lists.rst:78
msgid "Add to a List"
msgstr "Adicionar para uma lista"

#: ../../source/lists.rst:80
msgid ""
"To add titles to an existing list click on the list name from the page of "
"lists"
msgstr ""
"Para adicionar títulos para uma lista existente clicando no nome da lista na "
"página de listas"

#: ../../source/lists.rst:83
msgid "|image872|"
msgstr ""

#: ../../source/lists.rst:85
#, fuzzy
msgid "To open a list you can click the list name."
msgstr ""
"Para criar uma nova lista de usuários, clique no botão 'Nova lista de "
"usuários'"

#: ../../source/lists.rst:87
#, fuzzy
msgid ""
"From that page you can add titles by scanning barcodes into the box at the "
"bottom of the page"
msgstr ""
"Na página de Listas você pode adicionar títulos scaneando seus códigos de "
"barras na caixa na parte inferior da página"

#: ../../source/lists.rst:90
msgid "|image873|"
msgstr ""

#: ../../source/lists.rst:92
msgid ""
"A title can also be added to a list by selecting titles on the search "
"results page and choosing the list from the 'Add to' menu"
msgstr ""
"Um título também pode ser adicionado a uma lista selecionando os títulos na "
"página dos resultados de pesquisa e escolhendo a lista no menu 'Adicionar "
"para'"

#: ../../source/lists.rst:95
msgid "|image874|"
msgstr ""

#: ../../source/lists.rst:100
msgid "Viewing Lists"
msgstr "Visualizar listas"

#: ../../source/lists.rst:102
msgid "To see the contents of a list, visit the Lists page on the staff client"
msgstr ""
"Para ver o conteúdo de uma lista, visite a página de Listas no Posto de "
"processamento técnico"

#: ../../source/lists.rst:104
msgid "|image875|"
msgstr ""

#: ../../source/lists.rst:106
msgid "Clicking on the 'List Name' will show the contents of the list"
msgstr "Clicando em 'Nome da Lista' exibirá todos os conteúdos da lista"

#: ../../source/lists.rst:108
msgid "|image876|"
msgstr ""

#: ../../source/lists.rst:110 ../../source/lists.rst:169
msgid "From this list of items you can perform several actions"
msgstr ""

#: ../../source/lists.rst:112
msgid "'New list' will allow you to create another list"
msgstr ""

#: ../../source/lists.rst:114
#, fuzzy
msgid ""
"'Edit' will allow you to edit the description and permissions for this list"
msgstr ""
"Clicando em 'Editar no hóspede' irá permitir que edite o item no registro "
"hóspede."

#: ../../source/lists.rst:117
msgid ""
"'Send list' will send the list to the email address you enter (:ref:`view "
"sample List email <example-email-from-list-label>`)"
msgstr ""

#: ../../source/lists.rst:120
msgid ""
"'Download list' will allow you to download the cart using one of 3 default "
"formats or your :ref:`CSV Profiles`"
msgstr ""

#: ../../source/lists.rst:123
msgid "'Print list' will present you with a printable version of the list"
msgstr ""

#: ../../source/lists.rst:125
msgid ""
"Using the filters at the top of each column you can find specific items in "
"your list."
msgstr ""

#: ../../source/lists.rst:131
msgid "Merging Bibliographic Records Via Lists"
msgstr "Unindo registros bibliográficos a partir das Listas"

#: ../../source/lists.rst:133
#, fuzzy
msgid ""
"One way to merge together duplicate bibliographic records is to add them to "
"a list and use the Merge Tool from there."
msgstr ""
"A forma mais simples de unificar registros bibliográficos duplicados é "
"adicioná-los a uma lista e usar a ferramenta de unificação."

#: ../../source/lists.rst:136
msgid "|image877|"
msgstr ""

#: ../../source/lists.rst:138
msgid ""
"Once you have selected the records to merge together the process is the same "
"as if you had chosen to :ref:`merge via cataloging <merging-records-label>`."
msgstr ""

#: ../../source/lists.rst:144
msgid "Cart"
msgstr "Carrinho"

#: ../../source/lists.rst:146
msgid ""
"The cart is a temporary holding place for items in the OPAC and/or staff "
"client. The cart will be emptied once the session is ended (by closing the "
"browser or logging out). The cart is best used for performing batch "
"operations (holds, printing, emailing) or for getting a list of items to be "
"printed or emailed to yourself or a patron."
msgstr ""

#: ../../source/lists.rst:152
msgid ""
"If you would like to enable the cart in the staff client, you need to set "
"the :ref:`intranetbookbag` system preference to 'Show.' To add things to the "
"cart, search the catalog and select the items you would like added to your "
"cart and choose 'Cart' from the 'Add to' menu"
msgstr ""

#: ../../source/lists.rst:158
msgid "|image878|"
msgstr ""

#: ../../source/lists.rst:160
msgid ""
"A confirmation will appear below the cart button at the top of the staff "
"client"
msgstr ""

#: ../../source/lists.rst:163
msgid "|image879|"
msgstr ""

#: ../../source/lists.rst:165
#, fuzzy
msgid ""
"Clicking on the Cart icon will provide you with the contents of the cart"
msgstr "Clicando em 'Nome da Lista' exibirá todos os conteúdos da lista"

#: ../../source/lists.rst:167
msgid "|image880|"
msgstr ""

#: ../../source/lists.rst:171
msgid "'More details' will show more information about the items in the cart"
msgstr ""

#: ../../source/lists.rst:173
msgid ""
"'Send' will send the list to the email address you enter (:ref:`view sample "
"Cart email <example-email-from-cart-label>`)"
msgstr ""

#: ../../source/lists.rst:176
msgid ""
"'Download' will allow you to download the cart using one of 3 default "
"formats or your :ref:`CSV Profiles`"
msgstr ""

#: ../../source/lists.rst:179
msgid "'Print' will present you with a printable version of the cart"
msgstr ""

#: ../../source/lists.rst:181
msgid "'Empty and Close' will empty the list and close the window"
msgstr ""

#: ../../source/lists.rst:183
#, fuzzy
msgid "'Hide Window' will close the window"
msgstr "Enviar e fechar esta janela"
